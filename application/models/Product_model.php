<?php define ('BASEPATH') OR exit ('No direct script allowed');

class Product_model extends CI_Model
{
    private $table = "barang";

    public $id;
    public $kdbrg;
    public $brg;
    public $harga;
    public $stock;
    public $tanggal;
    public $category_id;
    public $id_satuan;
    public $total_terjual;

    public function rules() 
    {
        return[
            ['field' => 'kdbrg',
            'label' => 'Kdbrg',
            'rules' => 'required'],

            ['field' => 'nmbrg',
            'label' => 'Nmbrg',
            'rules' => 'required'],

            ['field' => 'harga',
            'label' => 'Harga',
            'rules' => 'numeric'],

            ['field' => 'stock',
            'label' => 'Stock',
            'rules' => 'numeric'],

            ['field' => 'tanggal',
            'label' => 'Tanggal',
            'rules' => 'datetime'],

            ['field' => 'category_id',
            'label' => 'Category_id',
            'rules' => 'numeric'],

            ['field' => 'id_satuan',
            'label' => 'Id_satuan',
            'rules' => 'numeric'],

            ['field' => 'total_terjual',
            'label' => 'Total_terjual',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get_where($this->barang, ["id"])->row();     
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id = uniqid();
        $this->kdbrg = $get["kdbrg"];
        $this->nmbrg = $post["nmbrg"];
        $this->harga = $harga["harga"];
        $this->stock = $stock["stock"];
        $this->tanggal = date(d/m/Y);
        $this->category_id = $get["category_id"];
        $this->id_satuan = $get["id_satuan"];
        $this->total_terjual = $post["total_terjual"];
        $this->db->insert($this->barang, $this);

    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->kdbrg = $get["kdbrg"];
        $this->nmbrg = $post["nmbrg"];
        $this->harga = $harga["harga"];
        $this->stock = $stock["stock"];
        $this->tanggal = date(d/m/Y);
        $this->category_id = $get["category_id"];
        $this->id_satuan = $get["id_satuan"];
        $this->total_terjual = $post["total_terjual"];
        $this->db->update($this->barang, $this, array('id' => $post['id']))
    }

    public function delete($id)
    {
        return $this->db->delete($this->barang, array("id" => $id));
    }
}

?>